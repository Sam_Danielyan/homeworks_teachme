# You are given four real numbers - the coordinates of two points on a
# plane. The first two numbers are the x and y coordinates of the first point,
# and the last two numbers are the x and y coordinates of the second point.
# Output the length of the line segment bounded by the two points.
print("Exercise 9")

x1 = float(input("Enter x1: "))
y1 = float(input("Enter x2: "))
x2 = float(input("Enter y1: "))
y2 = float(input("Enter y2: "))

x2 -= x1    # x2-x1
x2 = x2**2  # x^2
y2 -= y1    # y2-y1
y2 = y2**2  # y^2

x2 += y2        # x^2 + y^2
x2 = x2**0.5    # sqrt()

print(x2)

