#Given a real number, round it to the nearest whole.
print("Exercise 11")

fl_num = float(input("Enter your float: "))
y = int(fl_num)

diff = fl_num - y

if diff >= 0.5 :
    print(y+1)
else:
    print(y)