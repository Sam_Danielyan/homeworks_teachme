# Input three integers. Output the word “Sorted” if the numbers are listed in
# a non-increasing or non-decreasing order and “Unsorted” otherwise.
# Sample Input Sample Output
# 1 2 3 Sorted
# 1 3 2 Unsorted
# 5 0 -4 Sorted
# 9 9 9 Sorted
# 9 9 0 Sorted
print("Exercise 8")

a = int(input("Enter a: "))
b = int(input("Enter b: "))
c = int(input("Enter c: "))

if (a<=b and b <=c) or (a>=b and b>=c):
    print("Sorted")
else:
    print("Unsorted")
