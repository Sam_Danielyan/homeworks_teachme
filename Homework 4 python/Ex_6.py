# Input two positive integers, and output
#  a line describing their relation.
# Follow the sample format.
print("Exercise 6")

a = int(input("Enter first: "))
b = int(input("Enter second: "))

if a>b:
    print(a," > ",b)
elif a<b:
    print(a," < ",b)
else:
    print(a," = ",b)