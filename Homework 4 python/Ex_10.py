# Given the salaries of three employees working at a department, find the
# amount of money by which the salary of the highest-paid employee differs
# from the salary of the lowest-paid employee. The input consists of three
# positive integers - the salaries of the employees. Output a single number,
# the difference between the top and the bottom salaries
print("Exercise 10")

a1 = int(input("Enter 1: "))
a2 = int(input("Enter 2: "))
a3 = int(input("Enter 3: "))

max = max(a1,a2,a3)
min = min(a1,a2,a3)
print(max-min)
