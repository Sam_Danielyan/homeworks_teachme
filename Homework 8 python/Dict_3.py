#Write a python function, which create a dictionary
#for given number N, where keys are numbers from
#1 to N, and values are cubs of that numbers

def create(n):
    d_cubs = {}
    for i in range(1,n+1):
        d_cubs[i] = i**3
    
    return d_cubs

num = int(input("Enter N: "))
print(create(num))
