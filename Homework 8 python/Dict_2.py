#Write a python program which concat 2 dicts

def conc_dicts(d1,d2):
    for x,y in d2.items():
        d1[x] = y
    return d1

d_name = { "Ani":10, "Arman":10, "Tigran":9}
d_surname = { "Brown":7 ,"Bill":8 ,"Ramzey":7}
print(conc_dicts(d_name,d_surname))