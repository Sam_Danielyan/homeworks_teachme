#Count all letters, digits, and special symbols from a given string
print("Ex.2 Count")

sample = "P@#yn26at^&i5ve"
letters = 0
digits = 0 
symbols = 0

for i in sample:
    if i.isdigit():
        digits += 1
    elif (ord(i)>=65 and ord(i)<=90) or (ord(i)>=97 and ord(i)<=122):
        letters += 1
    else:
        symbols += 1
        
print("Total counts of chars, digits, and symbols")     
print("Chars =",letters)
print("Digits =",digits)
print("Symbols =",symbols)