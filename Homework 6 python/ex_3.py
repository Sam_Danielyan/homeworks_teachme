# Write a program to check if two strings are balanced. For example, strings s1 and
# s2 are balanced if all the characters in the s1 are present in s2. The character’s
# position doesn’t matter.
print("Ex.3 Balance")

print("Input 2 strings")
s1 = input("Enter first: ")
s2 = input("Enter second: ")

# s1 = "Ynf"
# s2 = "PYnative"

for i in s1:
    exist = False
    if i in s2:
        exist = True

if exist:
    print("True")
else:
    print("False")