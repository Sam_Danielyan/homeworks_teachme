#Write python function which return 
# whether given number is prime or not.

def isprime(x):
    prime = True

    for i in range(2,x//2 + 1,1):
        if x % i == 0:
            prime = False
            break
    
    if prime:
        return True
    else:
        return False


for i in range(2,100):
    if isprime(i):
        print(i)