# Create a string made of the first, middle 
# & last character of given string with odd number of
# symbols
# e.g.
# Sample = ‘Sevak’
# Expected ‘Svk’

from audioop import add


print("Exercise 8")

str1 = input("Enter your string: ")
size = len(str1)

if size % 2 ==1 :
    first = str1[0]
    last = str1[-1]
    middle = str1[(size-1)//2]
    new = first + middle + last
    print(new)
    