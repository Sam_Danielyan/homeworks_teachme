# Write a Python program to remove the n-th index character from a nonempty string.
# e.g.
# Sample string: ‘abcdefgh’
# n - 3
# Expected result: abcefgh
print("Exercise 3")
str1 = input("Enter your string: ")
n = int(input("Enter the index: "))
new_string = str1[:n] + str1[n+1:]
print(new_string)