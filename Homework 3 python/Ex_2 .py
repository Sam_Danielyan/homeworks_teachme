#Write a Python program to add 'ing' at the end of a given string 
# (length should be at least 3). If
#the given string already ends with 'ing' then add 
# 'ly' instead. If the string length of the given string
#is less than 3, leave it unchanged.
# e.g.
# Sample String : 'abc'
# Expected Result : 'abcing'
# _______________________
# Sample String : 'string'
# Expected Result : 'stringly'
print("Exercise 2")
str1 = input("Enter your string: ")
size = len(str1)

if size >= 3 :

    if str1[-3:] != "ing" :
        new_string = str1 + "ing"
        print(new_string)

    else :  #elif str[-3:] == "ing" :
        new_string = str1 + "ly"
        print(new_string)

