# Given dictionary is consisted of vehicles and
# their weights in kilograms. Construct a list
# of the names of vehicles with weight below 5000
# kilograms. In the same list comprehension make
# the key names all upper case.

dict1 = {"Audi":5200 , "Mercedes":4800 , "Opel":2500 , "Mazda":3500}
dict1["Toyota"] = 6000
dict1['Porshce'] = 5000
dict1['Bentley'] = 7000

new_list = [k.upper() for k,y in dict1.items() if y<= 5000]
print(new_list)

