# Write a list comprehension to print this output, without using mul
# *
# **
# ***
# ****
# *****
# ******
# *******
# ********
# *********

for x in range(1,8):
    print("*"*x)
