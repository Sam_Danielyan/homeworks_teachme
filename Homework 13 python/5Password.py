#Password length must be 10 characters long.
#It must contain at least 2 upper case letters,
#  1 digit, and 1 special symbol.


from random import choice
from random import shuffle

lower = "qwertyuiopasdfghjklzxcvbnm"
biger = "QWERTYUIOPASDFGHJKLZXCVBNM"
digit = "0123456789"
symbol = "!@~#$%^&*()}{_+"
result = f'{choice(biger)}{choice(biger)}{choice(digit)}{choice(symbol)}'

ans = lower + biger + digit +symbol
for i in range(6):
    result += choice(ans)

list_1 = []
for char in result:
    list_1.append(char)


shuffle(list_1)
new = "".join(list_1)
print(new)