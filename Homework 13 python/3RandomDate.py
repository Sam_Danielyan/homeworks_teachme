#Generate a random date between given start and end dates
from random import randrange as rand
import datetime as date

start = "2001-03-22"
end = date.datetime.now()
str_end = end.strftime("%Y-%m-%d")

year = rand(int(start[:4]),int(str_end[:4])-1)
month = rand(1,12)
day = rand(1,28)
print(f"{year}-{month}-{day}")

