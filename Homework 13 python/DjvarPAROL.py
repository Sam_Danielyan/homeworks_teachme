#Password length must be 10 characters long.
#It must contain at least 2 upper case letters,
#  1 digit, and 1 special symbol.

import random as rnd
def get_password():
    lower = "qwertyuiopasdfghjklzxcvbnm"
    biger = "QWERTYUIOPASDFGHJKLZXCVBNM"
    digit = "0123456789"
    symbol = "!@~#$%^&*(){}_+"

    password = ""
    ans = lower + biger + digit + symbol
    for i in range(10):
        password += (rnd.choice(ans))
    return password


x = get_password()

def check(word):
    upper = 0
    digit = 0
    symbol = 0
    for x in word:
        if x.isupper():
            upper +=1
        elif x.isdigit():
            digit +=1
        elif x.islower():
            pass
        else:
            symbol +=1

    if upper >=2 and digit >= 1 and symbol >=1:
        return True
    else:
        return False

if check(x):
    print(x)
else:
    ##REKURSIAYOV GREL
    print("__Sxal parol__",x)
