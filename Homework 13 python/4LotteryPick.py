# Random Lottery Pick. Generate 100 random 
# lottery tickets and pick
# two lucky tickets from it as a winner

from random import randrange
from random import sample

tickets = []
while len(tickets) != 100:
    x = randrange(1000000000,9999999999)
    if x not in tickets:
        tickets.append(x)

w = sample(tickets,3)

print("Winner Tickets are here: ->")
for i in range(3):
    print(i+1,":",w[i])