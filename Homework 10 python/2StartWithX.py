# Write a Python program to find if 
# a given string starts with a given
# character using Lambda

given = input("Enter string: ")
char = input("Enter character: ")

func = lambda x: x[0]==char
print(func(given))