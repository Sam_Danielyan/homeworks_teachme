# Write a Python program to check
#  whether a given string is number or
# not using Lambda

given = input("Enter string: ")

is_num = lambda x: x.isdigit()

if is_num(given):
    print(f"{given} is digit")
else:
    print("Try again")