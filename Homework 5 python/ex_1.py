#  Write a Python program to multiply all the items in a list.
print("Ex.1 Multiply")

arr = [5, 7, 3, 9, 11]
multi = 1

for i in arr:
    multi *= i

print(multi)