#Write a Python program to get the largest number from a list.
print("Ex.2 Largest")

arr = [5, 7, 3, 9, 11]
large = arr[0]

for i in arr:
    if large < i:
        large = i

print(large)
