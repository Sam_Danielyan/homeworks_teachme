#Write a Python program to get the smallest number from a list.
print("Ex.3 Smallest")

arr = [5, 7, 3, 9, 11]
small = arr[0]

for i in arr:
    if small > i:
        small = i

print(small)
