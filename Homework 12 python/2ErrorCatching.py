# Write a python program to catch error 
# and print “Error is catched”
# You need to do it with 5 different Errors

try:
    file = open("New.txt")
except FileNotFoundError:
    print("Error is catched")


try:
    print(x)
except NameError:
    print("Error is catched")


try:
    ll = [4,5,6,7]
    print(ll[4])
except :
    print("Error is catched")